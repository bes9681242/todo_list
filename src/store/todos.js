import { reactive } from 'vue';

export const todosStore = reactive({
    tasks: JSON.parse(localStorage.getItem('tasks'))|| [], 
    findAll() {
        return this.tasks;
    },
    findOneById(id){
        return this.tasks.filter(task => task.id === id);
    },
    save(){
        localStorage.setItem('tasks', JSON.stringify(this.tasks));
    }
});