export default {
    tasks : localStorage.tasks ? JSON.parse(localStorage.tasks) : [],
    saveTasks () {
        localStorage.tasks = JSON.stringify(this.tasks);
    },
    getTasks () {
        return this.tasks;
    }
 };
